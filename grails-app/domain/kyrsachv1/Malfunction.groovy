package kyrsachv1

class Malfunction {
	
	Date dateCreated
	String description
	Boolean fixed
	Date fixed_at

    static constraints = {
		description blank: false
		fixed nullable: true
		fixed_at nullable: true, blank: true
    }
	
	static belongsTo = [car: Car]
	
}
