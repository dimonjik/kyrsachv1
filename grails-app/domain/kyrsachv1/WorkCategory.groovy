package kyrsachv1

class WorkCategory {
	String name
	String description
	
    static constraints = {
		name blank: false
    }
	
	static hasMany = [repairJobs: RepairJob]
}
