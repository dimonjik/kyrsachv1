package kyrsachv1

class Car {
	
	String number
	String brand
	String model
	String description
	String color
	Date dateCreated

    static constraints = {
			number size: 3..30, blank: false, unique: true
			brand blank: false
			model blank: false
    }
	
	static hasMany = [malfunctions: Malfunction, repairJobs: RepairJob]
	
	def getIdentifier(){
		return brand + " " + model + " (" + number + ")"
	}
}
