package kyrsachv1

class User {
  static constraints = {
    login unique:true
    password password:true
    role(inList:["user", "admin"])
  }  
  
  String login
  String password
  String name
  String role = "user"
  
  String toString(){
    name
  }
}
