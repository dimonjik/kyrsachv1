package kyrsachv1

class RepairJob {
	
	String description
	String price
	Boolean finished
	Date finished_at
	
	static belongsTo = [car: Car, malfunction: Malfunction, workcategory: WorkCategory]
	
    static constraints = {
		finished_at nullable: true, blank: true
    }
}
