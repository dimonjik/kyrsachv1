
<%@ page import="kyrsachv1.RepairJob" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'repairJob.label', default: 'RepairJob')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl>
				
					<g:if test="${repairJobInstance?.car}">
						<dt><g:message code="repairJob.car.label" default="Car" /></dt>
						
							<dd><g:link controller="car" action="show" id="${repairJobInstance?.car?.id}">${repairJobInstance?.car?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${repairJobInstance?.description}">
						<dt><g:message code="repairJob.description.label" default="Description" /></dt>
						
							<dd><g:fieldValue bean="${repairJobInstance}" field="description"/></dd>
						
					</g:if>
				
					<g:if test="${repairJobInstance?.finished}">
						<dt><g:message code="repairJob.finished.label" default="Finished" /></dt>
						
							<dd><g:formatBoolean boolean="${repairJobInstance?.finished}" /></dd>
						
					</g:if>
				
					<g:if test="${repairJobInstance?.finished_at}">
						<dt><g:message code="repairJob.finished_at.label" default="Finishedat" /></dt>
						
							<dd><g:formatDate date="${repairJobInstance?.finished_at}" /></dd>
						
					</g:if>
				
					<g:if test="${repairJobInstance?.malfunction}">
						<dt><g:message code="repairJob.malfunction.label" default="Malfunction" /></dt>
						
							<dd><g:link controller="malfunction" action="show" id="${repairJobInstance?.malfunction?.id}">${repairJobInstance?.malfunction?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
					<g:if test="${repairJobInstance?.price}">
						<dt><g:message code="repairJob.price.label" default="Price" /></dt>
						
							<dd><g:fieldValue bean="${repairJobInstance}" field="price"/></dd>
						
					</g:if>
				
					<g:if test="${repairJobInstance?.workcategory}">
						<dt><g:message code="repairJob.workcategory.label" default="Workcategory" /></dt>
						
							<dd><g:link controller="workCategory" action="show" id="${repairJobInstance?.workcategory?.id}">${repairJobInstance?.workcategory?.encodeAsHTML()}</g:link></dd>
						
					</g:if>
				
				</dl>

				<g:form>
					<g:hiddenField name="id" value="${repairJobInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn" action="edit" id="${repairJobInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
