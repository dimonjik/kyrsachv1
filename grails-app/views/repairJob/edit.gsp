<%@ page import="kyrsachv1.RepairJob" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'repairJob.label', default: 'RepairJob')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">

			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<g:hasErrors bean="${repairJobInstance}">
				<bootstrap:alert class="alert-error">
				<ul>
					<g:eachError bean="${repairJobInstance}" var="error">
					<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
					</g:eachError>
				</ul>
				</bootstrap:alert>
				</g:hasErrors>

				<fieldset>
					<g:form class="form-horizontal" action="edit" id="${repairJobInstance?.id}" >
						<g:hiddenField name="version" value="${repairJobInstance?.version}" />
						<fieldset>
<%--							<f:all bean="repairJobInstance"/>--%>
							<f:with bean="repairJobInstance">
								<div class="row-fluid">
									<div class="span5">
										<f:field property="car" input-optionValue="identifier" />
										<f:field property="malfunction">
											<g:select id="malfunction" name="malfunction.id" from="${repairJobInstance.car.malfunctions}" optionKey="id" optionValue="${{it.description}}" required="" value="${malfunctionInstance?.car?.brand}" class="many-to-one"/>
										</f:field>
										<f:field property="workcategory" input-optionValue="name"/>
										<br/>
										<f:field property="finished" />
										<f:field property="finished_at" />
										<f:field property="price" />
									</div>
									
									<div class="span4">
										<f:field property="description">
											<g:textArea name="description" value="${value}" rows="6" style="width:250px"/>
										</f:field>
									</div>
								</div>								
							</f:with>
							<div class="form-actions">
								<button type="submit" class="btn btn-primary">
									<i class="icon-ok icon-white"></i>
									<g:message code="default.button.update.label" default="Update" />
								</button>
								<button type="submit" class="btn btn-danger" name="_action_delete" formnovalidate>
									<i class="icon-trash icon-white"></i>
									<g:message code="default.button.delete.label" default="Delete" />
								</button>
							</div>
						</fieldset>
					</g:form>
				</fieldset>

			</div>

		</div>
	</body>
</html>
