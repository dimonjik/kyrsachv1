
<%@ page import="kyrsachv1.RepairJob" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'repairJob.label', default: 'RepairJob')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li class="active">
							<g:link class="list" action="list">
								<i class="icon-list icon-white"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
<%--						<li>--%>
<%--							<g:link class="create" action="create">--%>
<%--								<i class="icon-plus"></i>--%>
<%--								<g:message code="default.create.label" args="[entityName]" />--%>
<%--							</g:link>--%>
<%--						</li>--%>
					</ul>
				</div>
			</div>

			<div class="span9">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
					<thead>
						<tr>
						
							<th class="header">
								<g:message code="repairJob.car.label" default="Car" />
								<g:message code="repairJob.malfunction.label" default="Malfunction" />
							</th>
							
							<g:sortableColumn property="finished_at" title="${message(code: 'repairJob.workcategory.label', default: 'Work category')}" />
							
							<g:sortableColumn property="finished_at" title="${message(code: 'repairJob.finished_at.label', default: 'Completed on')}" />	
						
							<g:sortableColumn property="price" title="${message(code: 'repairJob.price.label', default: 'Price')}" />
						
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${repairJobInstanceList}" var="repairJobInstance">
						
						<g:if test="${repairJobInstance.finished}">
							<tr class="success">
						</g:if>
						<g:else>
							<tr class="error">
						</g:else>
						
							<td class="link">
								<g:link action="show" id="${repairJobInstance.id}">
									${fieldValue(bean: repairJobInstance, field: "car.identifier")}
									-
									${fieldValue(bean: repairJobInstance, field: "malfunction.description")}
								</g:link>
							</td>
							
							<td>${fieldValue(bean:repairJobInstance, field: "workcategory.name") }</td>
						
							<td><g:formatDate date="${repairJobInstance.finished_at}" /></td>							
						
							<td>${fieldValue(bean: repairJobInstance, field: "price")}</td>
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${repairJobInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
