<%@ page import="kyrsachv1.RepairJob" %>



<div class="fieldcontain ${hasErrors(bean: repairJobInstance, field: 'car', 'error')} required">
	<label for="car">
		<g:message code="repairJob.car.label" default="Car" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="car" name="car.id" from="${kyrsachv1.Car.list()}" optionKey="id" required="" value="${repairJobInstance?.car?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: repairJobInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="repairJob.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${repairJobInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: repairJobInstance, field: 'finished', 'error')} ">
	<label for="finished">
		<g:message code="repairJob.finished.label" default="Finished" />
		
	</label>
	<g:checkBox name="finished" value="${repairJobInstance?.finished}" />
</div>

<div class="fieldcontain ${hasErrors(bean: repairJobInstance, field: 'finished_at', 'error')} required">
	<label for="finished_at">
		<g:message code="repairJob.finished_at.label" default="Finishedat" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="finished_at" precision="day"  value="${repairJobInstance?.finished_at}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: repairJobInstance, field: 'malfunction', 'error')} required">
	<label for="malfunction">
		<g:message code="repairJob.malfunction.label" default="Malfunction" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="malfunction" name="malfunction.id" from="${kyrsachv1.Malfunction.list()}" optionKey="id" required="" value="${repairJobInstance?.malfunction?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: repairJobInstance, field: 'price', 'error')} ">
	<label for="price">
		<g:message code="repairJob.price.label" default="Price" />
		
	</label>
	<g:textField name="price" value="${repairJobInstance?.price}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: repairJobInstance, field: 'workcategory', 'error')} required">
	<label for="workcategory">
		<g:message code="repairJob.workcategory.label" default="Workcategory" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="workcategory" name="workcategory.id" from="${kyrsachv1.WorkCategory.list()}" optionKey="id" required="" value="${repairJobInstance?.workcategory?.id}" class="many-to-one"/>
</div>

