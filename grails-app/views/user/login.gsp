<%@ page import="kyrsachv1.User"%>
<!doctype html>
<html>
<head>
<meta name="layout" content="bootstrap">
<g:set var="entityName"
	value="${message(code: 'user.label', default: 'User')}" />
<title>Sign in</title>
</head>
<body>
	<div class="row-fluid">

		<div class="span8 offset4">

			<div>
				<h1>
					Sign in
				</h1>
			</div>

			<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">
					${flash.message}
				</bootstrap:alert>
			</g:if>

			<fieldset>
				<g:form class="form-horizontal" action="authenticate">
					<table>
						<tbody>
							<tr class="prop">
								<td class="name"><label for="login">Login:</label></td>
								<td><input type="text" id="login" name="login" /></td>
							</tr>
							<tr class="prop">
								<td class="name"><label for="password">Password:</label></td>
								<td><input type="password" id="password" name="password" />
								</td>
							</tr>
							<tr>
								<td></td>
								<td><button type="submit" class="btn btn-primary"><g:message code="default.button.go.label" default="Go" /></button></td>
							</tr>
						</tbody>
					</table>
				</g:form>
			</fieldset>

		</div>

	</div>
</body>
</html>
