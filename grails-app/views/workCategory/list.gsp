
<%@ page import="kyrsachv1.WorkCategory" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'workCategory.label', default: 'WorkCategory')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li class="active">
							<g:link class="list" action="list">
								<i class="icon-list icon-white"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>

			<div class="span9">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
					<thead>
						<tr>
						
							<g:sortableColumn property="name" title="${message(code: 'workCategory.name.label', default: 'Name')}" />
						
							<g:sortableColumn property="description" title="${message(code: 'workCategory.description.label', default: 'Description')}" />
						
						</tr>
					</thead>
					<tbody>
					<g:each in="${workCategoryInstanceList}" var="workCategoryInstance">
						<tr>
						
							<td><g:link action="show" id="${workCategoryInstance.id}">${fieldValue(bean: workCategoryInstance, field: "name")}</g:link></td>
						
							<td>${fieldValue(bean: workCategoryInstance, field: "description")}</td>
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${workCategoryInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
