<%@ page import="kyrsachv1.WorkCategory" %>



<div class="fieldcontain ${hasErrors(bean: workCategoryInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="workCategory.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${workCategoryInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workCategoryInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="workCategory.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${workCategoryInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: workCategoryInstance, field: 'repairJobs', 'error')} ">
	<label for="repairJobs">
		<g:message code="workCategory.repairJobs.label" default="Repair Jobs" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${workCategoryInstance?.repairJobs?}" var="r">
    <li><g:link controller="repairJob" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="repairJob" action="create" params="['workCategory.id': workCategoryInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'repairJob.label', default: 'RepairJob')])}</g:link>
</li>
</ul>

</div>

