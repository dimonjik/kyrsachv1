
<%@ page import="kyrsachv1.Malfunction" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'malfunction.label', default: 'Malfunction')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li class="active">
							<g:link class="list" action="list">
								<i class="icon-list icon-white"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>

			<div class="span9">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
					<thead>
						<tr>
						
							<th class="header"><g:message code="malfunction.car.label" default="Car" /></th>
							
							<g:sortableColumn property="description" title="${message(code: 'malfunction.description.label', default: 'Description')}" />
						
							<g:sortableColumn property="created_at" title="${message(code: 'malfunction.created_at.label', default: 'Created')}" />
						
							<g:sortableColumn property="fixed_at" title="${message(code: 'malfunction.fixed_at.label', default: 'Fixedat')}" />
						
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${malfunctionInstanceList}" var="malfunctionInstance">
						<g:if test="${malfunctionInstance.fixed}">
							<tr class="success">
						</g:if>
						<g:else>
							<tr class="error">
						</g:else>						
						
							<td><g:link action="show" id="${malfunctionInstance.id}">
								${fieldValue(bean: malfunctionInstance, field: "car.brand")}
								${fieldValue(bean: malfunctionInstance, field: "car.model")}
								( ${fieldValue(bean: malfunctionInstance, field: "car.number")} )
							
							</g:link></td>
							
							<td>${fieldValue(bean: malfunctionInstance, field: "description")}</td>
						
							<td><g:formatDate date="${malfunctionInstance.dateCreated}" /></td>						
						
							<td><g:formatDate date="${malfunctionInstance.fixed_at}" /></td>
						
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${malfunctionInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
