<%@ page import="kyrsachv1.Malfunction" %>



<div class="fieldcontain ${hasErrors(bean: malfunctionInstance, field: 'car', 'error')} required">
	<label for="car">
		<g:message code="malfunction.car.label" default="Car" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="car" name="car.id" from="${kyrsachv1.Car.list()}" optionKey="id" required="" value="${malfunctionInstance?.car?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: malfunctionInstance, field: 'created_at', 'error')} required">
	<label for="created_at">
		<g:message code="malfunction.created_at.label" default="Createdat" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="created_at" precision="day"  value="${malfunctionInstance?.created_at}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: malfunctionInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="malfunction.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${malfunctionInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: malfunctionInstance, field: 'fixed', 'error')} ">
	<label for="fixed">
		<g:message code="malfunction.fixed.label" default="Fixed" />
		
	</label>
	<g:checkBox name="fixed" value="${malfunctionInstance?.fixed}" />
</div>

<div class="fieldcontain ${hasErrors(bean: malfunctionInstance, field: 'fixed_at', 'error')} required">
	<label for="fixed_at">
		<g:message code="malfunction.fixed_at.label" default="Fixedat" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="fixed_at" precision="day"  value="${malfunctionInstance?.fixed_at}"  />
</div>

