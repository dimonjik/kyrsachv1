
<%@ page import="kyrsachv1.Malfunction" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'malfunction.label', default: 'Malfunction')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>

				<dl>
				
					<g:if test="${malfunctionInstance?.car}">
						<dt><g:message code="malfunction.car.label" default="Car" /></dt>
						
							<dd><g:link controller="car" action="show" id="${malfunctionInstance?.car?.id}">
								${malfunctionInstance?.car.brand?.encodeAsHTML()} ${malfunctionInstance?.car.model?.encodeAsHTML()} 
								( ${malfunctionInstance?.car.number?.encodeAsHTML()})
							</g:link></dd>
						
					</g:if>
				
					<g:if test="${malfunctionInstance?.dateCreated}">
						<dt><g:message code="malfunction.dateCreated.label" default="Created" /></dt>
						
							<dd><g:formatDate date="${malfunctionInstance?.dateCreated}" /></dd>
						
					</g:if>
				
					<g:if test="${malfunctionInstance?.description}">
						<dt><g:message code="malfunction.description.label" default="Description" /></dt>
						
							<dd><g:fieldValue bean="${malfunctionInstance}" field="description"/></dd>
						
					</g:if>
				
					<g:if test="${malfunctionInstance?.fixed}">
						<dt><g:message code="malfunction.fixed.label" default="Fixed" /></dt>
						
							<dd><g:formatBoolean boolean="${malfunctionInstance?.fixed}" /></dd>
						
					</g:if>
				
					<g:if test="${malfunctionInstance?.fixed_at}">
						<dt><g:message code="malfunction.fixed_at.label" default="Fixedat" /></dt>
						
							<dd><g:formatDate date="${malfunctionInstance?.fixed_at}" /></dd>
						
					</g:if>
				
				</dl>

				<g:form>
					<g:hiddenField name="id" value="${malfunctionInstance?.id}" />
					<div class="form-actions">
						<g:link class="btn" action="edit" id="${malfunctionInstance?.id}">
							<i class="icon-pencil"></i>
							<g:message code="default.button.edit.label" default="Edit" />
						</g:link>
						<button class="btn btn-danger" type="submit" name="_action_delete">
							<i class="icon-trash icon-white"></i>
							<g:message code="default.button.delete.label" default="Delete" />
						</button>
					</div>
				</g:form>

			</div>

		</div>
	</body>
</html>
