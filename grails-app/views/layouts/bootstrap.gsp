<%@ page import="org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes" %>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="${meta(name: 'app.name')}"/></title>
		<meta name="description" content="">
		<meta name="author" content="">

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<r:require modules="scaffolding"/>

		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">

		<g:layoutHead/>
		<r:layoutResources/>
	</head>

	<body>
		<nav class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container-fluid">
					
					<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>
					
					<g:link class="brand" controller="Car">SQRT CarService</g:link>

					<div class="nav-collapse">
						<ul class="nav">
							<li><g:link controller="Car">Car DB</g:link></li>
							<li><g:link controller="Malfunction">Malfunctions list</g:link></li>
							<li><g:link controller="RepairJob">Current Jobs</g:link></li>
							<li class="divider-vertical"></li>
							<li><g:link controller="WorkCategory" class="pull-right">Work categories</g:link></li>
							<li class="divider-vertical"></li>
							<li>
								<g:if test="${session?.user?.name}">
									<g:link controller="User" action="logout" class="pull-right">
										<i>
											${session.user?.name},
										</i>
										logout																		
									</g:link>
								</g:if>
								<g:else>
									<g:link controller="User" action="login" class="pull-right">Login</g:link>
								</g:else>							
							</li>
							<li class="divider-vertical"></li>
							<g:if test="${session?.user?.role == "admin"}">
								<li><g:link controller="User">Users</g:link></li>
							</g:if>
<%--							<g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">--%>
<%--								<li<%= c.logicalPropertyName == controllerName ? ' class="active"' : '' %>><g:link controller="${c.logicalPropertyName}">${c.naturalName}</g:link></li>--%>
<%--							</g:each>--%>
						</ul>
					</div>
				</div>
			</div>
		</nav>

		<div class="container-fluid">
			<g:layoutBody/>

			<hr>

			<footer>
				<p>&copy; SQRT CarService 2013</p>
			</footer>
		</div>

		<r:layoutResources/>

	</body>
</html>