
<%@ page import="kyrsachv1.Car" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'car.label', default: 'Car')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li>
							<g:link class="list" action="list">
								<i class="icon-list"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>
			
			<div class="span9">

				<div class="page-header">
					<h1><g:message code="default.show.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<div class="row-fluid">
					<div class="span3">
						<dl>				
							<g:if test="${carInstance?.number}">
								<dt><g:message code="car.number.label" default="Number" /></dt>
								
									<dd><g:fieldValue bean="${carInstance}" field="number"/></dd>
								
							</g:if>
						
							<g:if test="${carInstance?.brand}">
								<dt><g:message code="car.brand.label" default="Brand" /></dt>
								
									<dd><g:fieldValue bean="${carInstance}" field="brand"/></dd>
								
							</g:if>
						
							<g:if test="${carInstance?.model}">
								<dt><g:message code="car.model.label" default="Model" /></dt>
								
									<dd><g:fieldValue bean="${carInstance}" field="model"/></dd>
								
							</g:if>
						
							<g:if test="${carInstance?.color}">
								<dt><g:message code="car.color.label" default="Color" /></dt>
								
									<dd><g:fieldValue bean="${carInstance}" field="color"/></dd>
								
							</g:if>
						
							<g:if test="${carInstance?.dateCreated}">
								<dt><g:message code="car.dateCreated.label" default="Added" /></dt>
								
									<dd><g:formatDate date="${carInstance?.dateCreated}"  format="kk:mm dd.MM.yy" /></dd>
								
							</g:if>
						
							<g:if test="${carInstance?.description}">
								<dt><g:message code="car.description.label" default="Description" /></dt>
								
									<dd><g:fieldValue bean="${carInstance}" field="description"/></dd>
								
							</g:if>
						</dl>
					</div>
					<div class="span4">
							<g:if test="${carInstance?.malfunctions}">
								<dt><g:message code="car.malfunctions.label" default="Malfunctions" /></dt>
								
									<g:each in="${carInstance.malfunctions}" var="m">
										<g:if test="${m.fixed}">
											<dd>
												<s>${m.description?.encodeAsHTML()}</s>
												<g:formatDate date="${m.fixed_at}"  format="dd.MM.yy" />
												
											</dd>
										</g:if>
										<g:else>
											<dd>
												${m.description?.encodeAsHTML()}
												<g:formatDate date="${m.dateCreated}"  format="dd.MM.yy" />
											</dd>
										</g:else>
									
									</g:each>
								
							</g:if>
						

					</div>
					<div class="span2">
						<g:if test="${carInstance?.repairJobs}">
							<dt><g:message code="car.repairJobs.label" default="Repair Jobs" /></dt>
							
								<g:each in="${carInstance.repairJobs}" var="r">
									<dd>
										<g:if test="${r.finished}">
											<s>${r.workcategory.name}</s>
											<g:formatDate date="${r.finished_at}"  format="dd.MM.yy" />
										</g:if>
										<g:else>
											${r.workcategory.name}
										</g:else>
									</dd>
								</g:each>								
						</g:if>
					</div>
				</div>
				

			</div>

		</div>
	</body>
</html>
