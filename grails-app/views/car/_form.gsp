<%@ page import="kyrsachv1.Car" %>



<div class="fieldcontain ${hasErrors(bean: carInstance, field: 'number', 'error')} required">
	<label for="number">
		<g:message code="car.number.label" default="Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="number" maxlength="30" required="" value="${carInstance?.number}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: carInstance, field: 'brand', 'error')} required">
	<label for="brand">
		<g:message code="car.brand.label" default="Brand" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="brand" required="" value="${carInstance?.brand}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: carInstance, field: 'model', 'error')} required">
	<label for="model">
		<g:message code="car.model.label" default="Model" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="model" required="" value="${carInstance?.model}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: carInstance, field: 'color', 'error')} ">
	<label for="color">
		<g:message code="car.color.label" default="Color" />
		
	</label>
	<g:textField name="color" value="${carInstance?.color}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: carInstance, field: 'created_at', 'error')} required">
	<label for="created_at">
		<g:message code="car.created_at.label" default="Createdat" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="created_at" precision="day"  value="${carInstance?.created_at}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: carInstance, field: 'description', 'error')} ">
	<label for="description">
		<g:message code="car.description.label" default="Description" />
		
	</label>
	<g:textField name="description" value="${carInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: carInstance, field: 'malfunctions', 'error')} ">
	<label for="malfunctions">
		<g:message code="car.malfunctions.label" default="Malfunctions" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${carInstance?.malfunctions?}" var="m">
    <li><g:link controller="malfunction" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="malfunction" action="create" params="['car.id': carInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'malfunction.label', default: 'Malfunction')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: carInstance, field: 'repairJobs', 'error')} ">
	<label for="repairJobs">
		<g:message code="car.repairJobs.label" default="Repair Jobs" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${carInstance?.repairJobs?}" var="r">
    <li><g:link controller="repairJob" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="repairJob" action="create" params="['car.id': carInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'repairJob.label', default: 'RepairJob')])}</g:link>
</li>
</ul>

</div>

