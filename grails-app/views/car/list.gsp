
<%@ page import="kyrsachv1.Car" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'car.label', default: 'Car')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="row-fluid">
			
			<div class="span3">
				<div class="well">
					<ul class="nav nav-list">
						<li class="nav-header">${entityName}</li>
						<li class="active">
							<g:link class="list" action="list">
								<i class="icon-list icon-white"></i>
								<g:message code="default.list.label" args="[entityName]" />
							</g:link>
						</li>
						<li>
							<g:link class="create" action="create">
								<i class="icon-plus"></i>
								<g:message code="default.create.label" args="[entityName]" />
							</g:link>
						</li>
					</ul>
				</div>
			</div>

			<div class="span9">
				
				<div class="page-header">
					<h1><g:message code="default.list.label" args="[entityName]" /></h1>
				</div>

				<g:if test="${flash.message}">
				<bootstrap:alert class="alert-info">${flash.message}</bootstrap:alert>
				</g:if>
				
				<table class="table table-striped">
					<thead>
						<tr>
						
							<g:sortableColumn property="number" title="${message(code: 'car.number.label', default: 'Number')}" />
						
							<g:sortableColumn property="brand" title="${message(code: 'car.brand.label', default: 'Brand')}" />
						
							<g:sortableColumn property="model" title="${message(code: 'car.model.label', default: 'Model')}" />
						
							<g:sortableColumn property="color" title="${message(code: 'car.color.label', default: 'Color')}" />
						
							<g:sortableColumn property="dateCreated" title="${message(code: 'car.dateCreated.label', default: 'Added')}" />
						
							<g:sortableColumn property="description" title="${message(code: 'car.description.label', default: 'Notice')}" />
						
							<th></th>
						</tr>
					</thead>
					<tbody>
					<g:each in="${carInstanceList}" var="carInstance">
						<tr>
							
							
							<td><g:link action="show" id="${carInstance.id}">${fieldValue(bean: carInstance, field: "number")}</g:link></td>
						
							<td>${fieldValue(bean: carInstance, field: "brand")}</td>
						
							<td>${fieldValue(bean: carInstance, field: "model")}</td>
						
							<td>${fieldValue(bean: carInstance, field: "color")}</td>
						
							<td><g:formatDate date="${carInstance.dateCreated}" format="dd.MM.yy" /></td>
						
							<td>${fieldValue(bean: carInstance, field: "description")}</td>
						
						</tr>
					</g:each>
					</tbody>
				</table>
				<div class="pagination">
					<bootstrap:paginate total="${carInstanceTotal}" />
				</div>
			</div>

		</div>
	</body>
</html>
