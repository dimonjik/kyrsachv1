package kyrsachv1

import org.springframework.dao.DataIntegrityViolationException


@Mixin(AuthService)
class CarController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']
	
	def beforeInterceptor = [action: this.&auth, except: ['show_client']]

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [carInstanceList: Car.list(params), carInstanceTotal: Car.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[carInstance: new Car(params)]
			break
		case 'POST':
	        def carInstance = new Car(params)
	        if (!carInstance.save(flush: true)) {
	            render view: 'create', model: [carInstance: carInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'car.label', default: 'Car'), carInstance.id])
	        redirect action: 'show', id: carInstance.id
			break
		}
    }

    def show() {
        def carInstance = Car.get(params.id)
        if (!carInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'car.label', default: 'Car'), params.id])
            redirect action: 'list'
            return
        }

        [carInstance: carInstance]
    }
	
	def show_client() {
		def carInstance = Car.get(params.id)
		if (!carInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'car.label', default: 'Car'), params.id])
			redirect action: 'list'
			return
		}

		[carInstance: carInstance]
	}

    def edit() {
		switch (request.method) {
		case 'GET':
	        def carInstance = Car.get(params.id)
	        if (!carInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'car.label', default: 'Car'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [carInstance: carInstance]
			break
		case 'POST':
	        def carInstance = Car.get(params.id)
	        if (!carInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'car.label', default: 'Car'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (carInstance.version > version) {
	                carInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'car.label', default: 'Car')] as Object[],
	                          "Another user has updated this Car while you were editing")
	                render view: 'edit', model: [carInstance: carInstance]
	                return
	            }
	        }

	        carInstance.properties = params

	        if (!carInstance.save(flush: true)) {
	            render view: 'edit', model: [carInstance: carInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'car.label', default: 'Car'), carInstance.id])
	        redirect action: 'show', id: carInstance.id
			break
		}
    }

    def delete() {
        def carInstance = Car.get(params.id)
        if (!carInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'car.label', default: 'Car'), params.id])
            redirect action: 'list'
            return
        }

        try {
            carInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'car.label', default: 'Car'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'car.label', default: 'Car'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
