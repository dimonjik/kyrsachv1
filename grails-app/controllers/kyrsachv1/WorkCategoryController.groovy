package kyrsachv1

import org.springframework.dao.DataIntegrityViolationException


@Mixin(AuthService)
class WorkCategoryController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']

	def beforeInterceptor = [action: this.&auth_admin]
	
    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [workCategoryInstanceList: WorkCategory.list(params), workCategoryInstanceTotal: WorkCategory.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[workCategoryInstance: new WorkCategory(params)]
			break
		case 'POST':
	        def workCategoryInstance = new WorkCategory(params)
	        if (!workCategoryInstance.save(flush: true)) {
	            render view: 'create', model: [workCategoryInstance: workCategoryInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'workCategory.label', default: 'WorkCategory'), workCategoryInstance.id])
	        redirect action: 'show', id: workCategoryInstance.id
			break
		}
    }

    def show() {
        def workCategoryInstance = WorkCategory.get(params.id)
        if (!workCategoryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'workCategory.label', default: 'WorkCategory'), params.id])
            redirect action: 'list'
            return
        }

        [workCategoryInstance: workCategoryInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def workCategoryInstance = WorkCategory.get(params.id)
	        if (!workCategoryInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'workCategory.label', default: 'WorkCategory'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [workCategoryInstance: workCategoryInstance]
			break
		case 'POST':
	        def workCategoryInstance = WorkCategory.get(params.id)
	        if (!workCategoryInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'workCategory.label', default: 'WorkCategory'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (workCategoryInstance.version > version) {
	                workCategoryInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'workCategory.label', default: 'WorkCategory')] as Object[],
	                          "Another user has updated this WorkCategory while you were editing")
	                render view: 'edit', model: [workCategoryInstance: workCategoryInstance]
	                return
	            }
	        }

	        workCategoryInstance.properties = params

	        if (!workCategoryInstance.save(flush: true)) {
	            render view: 'edit', model: [workCategoryInstance: workCategoryInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'workCategory.label', default: 'WorkCategory'), workCategoryInstance.id])
	        redirect action: 'show', id: workCategoryInstance.id
			break
		}
    }

    def delete() {
        def workCategoryInstance = WorkCategory.get(params.id)
        if (!workCategoryInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'workCategory.label', default: 'WorkCategory'), params.id])
            redirect action: 'list'
            return
        }

        try {
            workCategoryInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'workCategory.label', default: 'WorkCategory'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'workCategory.label', default: 'WorkCategory'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
