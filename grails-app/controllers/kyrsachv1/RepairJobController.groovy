package kyrsachv1

import org.springframework.dao.DataIntegrityViolationException


@Mixin(AuthService)
class RepairJobController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']
	
	def beforeInterceptor = [action: this.&auth]

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [repairJobInstanceList: RepairJob.list(params), repairJobInstanceTotal: RepairJob.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[repairJobInstance: new RepairJob(params)]
			break
		case 'POST':
	        def repairJobInstance = new RepairJob(params)
	        if (!repairJobInstance.save(flush: true)) {
	            render view: 'create', model: [repairJobInstance: repairJobInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'repairJob.label', default: 'RepairJob'), repairJobInstance.id])
	        redirect action: 'show', id: repairJobInstance.id
			break
		}
    }

    def show() {
        def repairJobInstance = RepairJob.get(params.id)
        if (!repairJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'repairJob.label', default: 'RepairJob'), params.id])
            redirect action: 'list'
            return
        }

        [repairJobInstance: repairJobInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def repairJobInstance = RepairJob.get(params.id)
	        if (!repairJobInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'repairJob.label', default: 'RepairJob'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [repairJobInstance: repairJobInstance]
			break
		case 'POST':
	        def repairJobInstance = RepairJob.get(params.id)
	        if (!repairJobInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'repairJob.label', default: 'RepairJob'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (repairJobInstance.version > version) {
	                repairJobInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'repairJob.label', default: 'RepairJob')] as Object[],
	                          "Another user has updated this RepairJob while you were editing")
	                render view: 'edit', model: [repairJobInstance: repairJobInstance]
	                return
	            }
	        }

	        repairJobInstance.properties = params

	        if (!repairJobInstance.save(flush: true)) {
	            render view: 'edit', model: [repairJobInstance: repairJobInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'repairJob.label', default: 'RepairJob'), repairJobInstance.id])
	        redirect action: 'show', id: repairJobInstance.id
			break
		}
    }

    def delete() {
        def repairJobInstance = RepairJob.get(params.id)
        if (!repairJobInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'repairJob.label', default: 'RepairJob'), params.id])
            redirect action: 'list'
            return
        }

        try {
            repairJobInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'repairJob.label', default: 'RepairJob'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'repairJob.label', default: 'RepairJob'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
