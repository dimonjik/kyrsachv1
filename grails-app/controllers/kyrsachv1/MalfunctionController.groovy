package kyrsachv1

import org.springframework.dao.DataIntegrityViolationException


@Mixin(AuthService)
class MalfunctionController {

    static allowedMethods = [create: ['GET', 'POST'], edit: ['GET', 'POST'], delete: 'POST']
	
	def beforeInterceptor = [action: this.&auth]

    def index() {
        redirect action: 'list', params: params
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [malfunctionInstanceList: Malfunction.list(params), malfunctionInstanceTotal: Malfunction.count()]
    }

    def create() {
		switch (request.method) {
		case 'GET':
        	[malfunctionInstance: new Malfunction(params)]
			break
		case 'POST':
	        def malfunctionInstance = new Malfunction(params)
	        if (!malfunctionInstance.save(flush: true)) {
	            render view: 'create', model: [malfunctionInstance: malfunctionInstance]
	            return
	        }

			flash.message = message(code: 'default.created.message', args: [message(code: 'malfunction.label', default: 'Malfunction'), malfunctionInstance.id])
	        redirect action: 'show', id: malfunctionInstance.id
			break
		}
    }

    def show() {
        def malfunctionInstance = Malfunction.get(params.id)
        if (!malfunctionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'malfunction.label', default: 'Malfunction'), params.id])
            redirect action: 'list'
            return
        }

        [malfunctionInstance: malfunctionInstance]
    }

    def edit() {
		switch (request.method) {
		case 'GET':
	        def malfunctionInstance = Malfunction.get(params.id)
	        if (!malfunctionInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'malfunction.label', default: 'Malfunction'), params.id])
	            redirect action: 'list'
	            return
	        }

	        [malfunctionInstance: malfunctionInstance]
			break
		case 'POST':
	        def malfunctionInstance = Malfunction.get(params.id)
	        if (!malfunctionInstance) {
	            flash.message = message(code: 'default.not.found.message', args: [message(code: 'malfunction.label', default: 'Malfunction'), params.id])
	            redirect action: 'list'
	            return
	        }

	        if (params.version) {
	            def version = params.version.toLong()
	            if (malfunctionInstance.version > version) {
	                malfunctionInstance.errors.rejectValue('version', 'default.optimistic.locking.failure',
	                          [message(code: 'malfunction.label', default: 'Malfunction')] as Object[],
	                          "Another user has updated this Malfunction while you were editing")
	                render view: 'edit', model: [malfunctionInstance: malfunctionInstance]
	                return
	            }
	        }

	        malfunctionInstance.properties = params

	        if (!malfunctionInstance.save(flush: true)) {
	            render view: 'edit', model: [malfunctionInstance: malfunctionInstance]
	            return
	        }

			flash.message = message(code: 'default.updated.message', args: [message(code: 'malfunction.label', default: 'Malfunction'), malfunctionInstance.id])
	        redirect action: 'show', id: malfunctionInstance.id
			break
		}
    }

    def delete() {
        def malfunctionInstance = Malfunction.get(params.id)
        if (!malfunctionInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'malfunction.label', default: 'Malfunction'), params.id])
            redirect action: 'list'
            return
        }

        try {
            malfunctionInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'malfunction.label', default: 'Malfunction'), params.id])
            redirect action: 'list'
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'malfunction.label', default: 'Malfunction'), params.id])
            redirect action: 'show', id: params.id
        }
    }
}
