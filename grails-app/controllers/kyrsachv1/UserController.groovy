package kyrsachv1

@Mixin(AuthService)
class UserController {
	static scaffold = User
	
	def beforeInterceptor = [action: this.&auth_admin, except: ['login', 'authenticate']]
	
	def login = {}
	
	def authenticate = {
	  def user = User.findByLoginAndPassword(params.login, params.password)
	  if(user){
		session.user = user
		flash.message = "Hello ${user.name}!"
		redirect(controller:"car", action:"list")
	  }else{
		flash.message = "Sorry, ${params.login}. Please try again."
		redirect(action:"login")
	  }
	}
	
	def logout = {
	  flash.message = "Goodbye"
	  if (session.user) {flash.message += "${session.user.name}"}
	  session.user = null
	  redirect(controller:"car", action:"list")
	}
}
