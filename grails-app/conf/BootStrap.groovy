import kyrsachv1.User

class BootStrap {

    def init = { servletContext ->
		if (!User.count()) {
			def admin = new User(name: "Administrator", login: "admin", password: "admin", role: "admin")
			admin.save()
			
			def user = new User(name: "User", login: "user", password: "user", role: "user")
			user.save()
		}
    }
    def destroy = {
    }
}
