package kyrsachv1

class AuthService {

	def auth() {
		if(!session.user) {
		  redirect(controller:"user", action:"login")
		  return false
		}
	}
	
	def auth_admin(){
		if(!(session?.user?.role == "admin")) {
			flash.message = "Only for administrators"
			redirect(controller:"user", action:"login")
			return false
		  }
	}
}
