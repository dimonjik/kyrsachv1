package kyrsachv1



import org.junit.*
import grails.test.mixin.*

@TestFor(MalfunctionController)
@Mock(Malfunction)
class MalfunctionControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/malfunction/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.malfunctionInstanceList.size() == 0
        assert model.malfunctionInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.malfunctionInstance != null
    }

    void testSave() {
        controller.save()

        assert model.malfunctionInstance != null
        assert view == '/malfunction/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/malfunction/show/1'
        assert controller.flash.message != null
        assert Malfunction.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/malfunction/list'

        populateValidParams(params)
        def malfunction = new Malfunction(params)

        assert malfunction.save() != null

        params.id = malfunction.id

        def model = controller.show()

        assert model.malfunctionInstance == malfunction
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/malfunction/list'

        populateValidParams(params)
        def malfunction = new Malfunction(params)

        assert malfunction.save() != null

        params.id = malfunction.id

        def model = controller.edit()

        assert model.malfunctionInstance == malfunction
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/malfunction/list'

        response.reset()

        populateValidParams(params)
        def malfunction = new Malfunction(params)

        assert malfunction.save() != null

        // test invalid parameters in update
        params.id = malfunction.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/malfunction/edit"
        assert model.malfunctionInstance != null

        malfunction.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/malfunction/show/$malfunction.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        malfunction.clearErrors()

        populateValidParams(params)
        params.id = malfunction.id
        params.version = -1
        controller.update()

        assert view == "/malfunction/edit"
        assert model.malfunctionInstance != null
        assert model.malfunctionInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/malfunction/list'

        response.reset()

        populateValidParams(params)
        def malfunction = new Malfunction(params)

        assert malfunction.save() != null
        assert Malfunction.count() == 1

        params.id = malfunction.id

        controller.delete()

        assert Malfunction.count() == 0
        assert Malfunction.get(malfunction.id) == null
        assert response.redirectedUrl == '/malfunction/list'
    }
}
