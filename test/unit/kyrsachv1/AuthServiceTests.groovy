package kyrsachv1



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(AuthService)
class AuthServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
