package kyrsachv1



import org.junit.*
import grails.test.mixin.*

@TestFor(WorkCategoryController)
@Mock(WorkCategory)
class WorkCategoryControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/workCategory/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.workCategoryInstanceList.size() == 0
        assert model.workCategoryInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.workCategoryInstance != null
    }

    void testSave() {
        controller.save()

        assert model.workCategoryInstance != null
        assert view == '/workCategory/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/workCategory/show/1'
        assert controller.flash.message != null
        assert WorkCategory.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/workCategory/list'

        populateValidParams(params)
        def workCategory = new WorkCategory(params)

        assert workCategory.save() != null

        params.id = workCategory.id

        def model = controller.show()

        assert model.workCategoryInstance == workCategory
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/workCategory/list'

        populateValidParams(params)
        def workCategory = new WorkCategory(params)

        assert workCategory.save() != null

        params.id = workCategory.id

        def model = controller.edit()

        assert model.workCategoryInstance == workCategory
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/workCategory/list'

        response.reset()

        populateValidParams(params)
        def workCategory = new WorkCategory(params)

        assert workCategory.save() != null

        // test invalid parameters in update
        params.id = workCategory.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/workCategory/edit"
        assert model.workCategoryInstance != null

        workCategory.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/workCategory/show/$workCategory.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        workCategory.clearErrors()

        populateValidParams(params)
        params.id = workCategory.id
        params.version = -1
        controller.update()

        assert view == "/workCategory/edit"
        assert model.workCategoryInstance != null
        assert model.workCategoryInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/workCategory/list'

        response.reset()

        populateValidParams(params)
        def workCategory = new WorkCategory(params)

        assert workCategory.save() != null
        assert WorkCategory.count() == 1

        params.id = workCategory.id

        controller.delete()

        assert WorkCategory.count() == 0
        assert WorkCategory.get(workCategory.id) == null
        assert response.redirectedUrl == '/workCategory/list'
    }
}
