package kyrsachv1



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Malfunction)
class MalfunctionTests {

    void testSomething() {
       fail "Implement me"
    }
}
