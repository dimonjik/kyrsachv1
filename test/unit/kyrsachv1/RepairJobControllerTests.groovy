package kyrsachv1



import org.junit.*
import grails.test.mixin.*

@TestFor(RepairJobController)
@Mock(RepairJob)
class RepairJobControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/repairJob/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.repairJobInstanceList.size() == 0
        assert model.repairJobInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.repairJobInstance != null
    }

    void testSave() {
        controller.save()

        assert model.repairJobInstance != null
        assert view == '/repairJob/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/repairJob/show/1'
        assert controller.flash.message != null
        assert RepairJob.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/repairJob/list'

        populateValidParams(params)
        def repairJob = new RepairJob(params)

        assert repairJob.save() != null

        params.id = repairJob.id

        def model = controller.show()

        assert model.repairJobInstance == repairJob
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/repairJob/list'

        populateValidParams(params)
        def repairJob = new RepairJob(params)

        assert repairJob.save() != null

        params.id = repairJob.id

        def model = controller.edit()

        assert model.repairJobInstance == repairJob
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/repairJob/list'

        response.reset()

        populateValidParams(params)
        def repairJob = new RepairJob(params)

        assert repairJob.save() != null

        // test invalid parameters in update
        params.id = repairJob.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/repairJob/edit"
        assert model.repairJobInstance != null

        repairJob.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/repairJob/show/$repairJob.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        repairJob.clearErrors()

        populateValidParams(params)
        params.id = repairJob.id
        params.version = -1
        controller.update()

        assert view == "/repairJob/edit"
        assert model.repairJobInstance != null
        assert model.repairJobInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/repairJob/list'

        response.reset()

        populateValidParams(params)
        def repairJob = new RepairJob(params)

        assert repairJob.save() != null
        assert RepairJob.count() == 1

        params.id = repairJob.id

        controller.delete()

        assert RepairJob.count() == 0
        assert RepairJob.get(repairJob.id) == null
        assert response.redirectedUrl == '/repairJob/list'
    }
}
